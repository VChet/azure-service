const path = require("path");
global.__base = path.join(__dirname, "/");

let tObj = require("./config.dev");
console.log("process.env.NODE_ENV: ", process.env.NODE_ENV);

if (process.env.NODE_ENV === "production") tObj = require("./config.prod");

module.exports = {
  appPort: "7850",
  uriBase: "https://northeurope.api.cognitive.microsoft.com/face/v1.0/detect",
  subscriptionKey: "a054db5ff2634770917b3d2f4c36b5ab",
  ...tObj
};
