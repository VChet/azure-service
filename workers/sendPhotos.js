module.exports = { sendPhotos };

const fs = require("fs-extra");
const moment = require("moment");
require("request");
const rp = require("request-promise");

const config = require("../config");

function sleep(sleepDuration) {
  const now = new Date().getTime();
  while (new Date().getTime() < now + sleepDuration) {
    // wait sleepDuration
  }
}

function getFileSize(filename) {
  const stats = fs.statSync(filename);
  const fileSizeInKB = stats["size"] / 1000;
  return fileSizeInKB;
}

async function sendPhotos(req, res, next) {
  const params = {
    "returnFaceId": "false",
    "returnFaceLandmarks": "false",
    "returnFaceAttributes": "emotion"
  };

  const files = await fs.readdir("./photos/");
  const results = [];
  console.log(`[${moment().format("HH:mm:ss")}] Started sending requests`);
  await Promise.all(files.map(async file => {
    const options = {
      url: config.uriBase,
      headers: {
        "Content-Type": "application/octet-stream",
        "Ocp-Apim-Subscription-Key": config.subscriptionKey
      },
      qs: params,
      body: fs.readFileSync("./photos/" + file)
    };

    const started = Date.now();
    const response = await rp.post(options).catch(err => {
      if (err.statusCode === 401) {
        const error = "Invalid uriBase/subscriptionKey";
        console.log(`[${moment().format("HH:mm:ss")}] ${error}`);
        res.json({ error });
        process.exit(1);
      } else if (err.statusCode === 429) {
        console.error(`[${moment().format("HH:mm:ss")}] Limit is exceeded. Waiting 60 seconds before next request`);
        console.count("Limit");
        sleep(1000 * 60);
        console.log(`[${moment().format("HH:mm:ss")}] Sending request after timeout`);
      } else {
        console.log(err.statusCode, err.error);
      }
    });
    const finished = Date.now();
    if (response) {
      const data = JSON.parse(response);
      if (data.length === 0) {
        results.push({
          file: file,
          faceRectangle: "Face not found",
          happiness: "Face not found",
          size: `${getFileSize("./photos/" + file).toFixed(1)}kB`,
          started: moment(started).format("HH:mm:ss:SSS"),
          finished: moment(finished).format("HH:mm:ss:SSS"),
          execTime: moment(finished - started).format("ss:SSS")
        });
      } else {
        results.push({
          file: file,
          faceRectangle: data[0].faceRectangle,
          happiness: data[0].faceAttributes.emotion.happiness,
          size: `${getFileSize("./photos/" + file).toFixed(1)}kB`,
          started: moment(started).format("HH:mm:ss:SSS"),
          finished: moment(finished).format("HH:mm:ss:SSS"),
          execTime: moment(finished - started).format("ss:SSS")
        });
      }
    }
  }));
  fs.writeFileSync("./results.json", JSON.stringify(results, null, 2), err => {
    if (err) return console.log(err);
  });
  // console.log(results);
  console.log(`There are ${results.length} results`);
}
