const express = require("express");
const bodyParser = require("body-parser");
const multer = require("multer");
const upload = multer();

const config = require("./config");
const { sendPhotos } = require("./workers/sendPhotos");

const app = express();
const PORT = config.appPort;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(upload.any());

app.get("/", (req, res) => {
  res.send("Server is up");
});

app.post("/photo", sendPhotos);

app.set("port", PORT);

app.listen(app.get("port"), () => {
  console.log(`Server is up and running on port ${PORT}`);
});
